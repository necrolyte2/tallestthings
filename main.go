package main

import (
	"gitlab.com/necrolyte2/tallestthings/db"
	"gitlab.com/necrolyte2/tallestthings/http"
)

func main() {
	db, err := db.NewJSON("towersJSONDB")
	if err != nil {
		panic(err)
	}

	httpServer := http.NewRouter(db)
	httpServer.Start()
}
