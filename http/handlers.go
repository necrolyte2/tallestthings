package http

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/necrolyte2/tallestthings/models"
)

func (s server) insertTowerHandler(c *gin.Context) {
	var json models.Tower
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := json.Validate(); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if err := s.DB.Insert(json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"created": json})
}

func (s server) getTowerHandler(c *gin.Context) {
	towerName := c.Param("towerName")

	foundTower, err := s.DB.Get(models.Tower{Name: towerName})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("could not find %s: %s", towerName, err.Error())})
		return
	}

	c.JSON(http.StatusOK, foundTower)
}

func (s server) getAllTowerHandler(c *gin.Context) {
	towers, err := s.DB.GetAll()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("could not retrieve towers: %s", err.Error())})
		return
	}

	c.JSON(http.StatusOK, towers)
}
