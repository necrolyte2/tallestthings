package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/necrolyte2/tallestthings/db"
)

type server struct {
	router *gin.Engine
	DB     db.DB
}

// NewRouter creates a new router
func NewRouter(db db.DB) server {
	return server{
		router: gin.Default(),
		DB:     db,
	}
}

func (s server) routes() {
	s.router.POST("/api/v1/tower", s.insertTowerHandler)
	s.router.GET("/api/v1/tower/:towerName", s.getTowerHandler)
	s.router.GET("/api/v1/tower", s.getAllTowerHandler)
}

// Start will start the Gin router
func (s server) Start() {
	s.routes()
	s.router.Run()
}
