package db

import "gitlab.com/necrolyte2/tallestthings/models"

// DB is the main DB Interface
type DB interface {
	Insert(models.Tower) error
	Get(models.Tower) (models.Tower, error)
	GetAll() ([]models.Tower, error)
	// Update(models.Tower, models.Tower{}) (models.Tower{}, error)
	// Delete(models.Tower) (error)
}
