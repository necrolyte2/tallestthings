package db

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"

	"gitlab.com/necrolyte2/tallestthings/models"
)

// JSON is a JSON db implementation
type JSON struct {
	path string
}

// NewJSON is the "constructor" for JSON
func NewJSON(path string) (JSON, error) {
	return JSON{
		path: path,
	}, nil

}

// given a full filepath to a json file, get dirname of that path and ensure it exists
func (db JSON) ensurePath(fullPath string) error {
	createPath := fullPath
	// if path is a full path to a file with an extension then dirname the path
	if filepath.Ext(createPath) != "" {
		createPath = filepath.Dir(createPath)
	}
	return os.MkdirAll(createPath, 0755)
}

func (db JSON) writeJSONFile(content []byte, path string) error {
	err := db.ensurePath(path)
	if err != nil {
		return fmt.Errorf("error creating json root directory: %v", err)
	}
	if content[len(content)-1] != '\n' {
		content = append(content, '\n')
	}
	return ioutil.WriteFile(path, content, 0644)
}

func (db JSON) readJSONFile(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}

func (db JSON) recordID(record models.Tower) string {
	name := record.Name
	id := md5.Sum([]byte(name))
	return fmt.Sprintf("%x", id)
}

func (db JSON) jsonPath(record models.Tower) string {
	id := db.recordID(record)
	return filepath.Join(db.path, fmt.Sprintf("%s.json", id))
}

func (db JSON) recordFiles() []string {
	// Get all json files in db path
	matches, _ := filepath.Glob(filepath.Join(db.path, "*.json"))
	// Sort it just in case, but it should be sorted already
	sort.Strings(matches)
	return matches
}

// FileExists checks for file existance. Could be a bug here with permission denied, but .... I'm lazy
func FileExists(filePath string) bool {
	if _, err := os.Stat(filePath); err == nil {
		return true
	}
	return false
}

// Insert inserts a given record
func (db JSON) Insert(record models.Tower) error {
	// Ensure record doesn't exists for same id
	recordPath := db.jsonPath(record)
	if FileExists(recordPath) {
		return fmt.Errorf("record already exists")
	}

	json, err := json.Marshal(record)
	if err != nil {
		return fmt.Errorf("unable to marshal record to string: %v", err)
	}

	db.writeJSONFile(json, recordPath)
	if err != nil {
		return fmt.Errorf("error writing to csv backend: %v", err)
	}
	return nil
}

func (db JSON) readRecord(recordPath string) (models.Tower, error) {
	var towerRecord models.Tower
	if FileExists(recordPath) {
		data, err := db.readJSONFile(recordPath)
		if err != nil {
			return towerRecord, fmt.Errorf("error retrieving contents from db: %s", err.Error())
		}

		err = json.Unmarshal(data, &towerRecord)
		if err != nil {
			return towerRecord, fmt.Errorf("error converting contents into tower object: %s", err.Error())
		}

		return towerRecord, nil
	}
	return towerRecord, fmt.Errorf("no record found for path %s", recordPath)
}

// Get will get a record
func (db JSON) Get(record models.Tower) (models.Tower, error) {
	recordPath := db.jsonPath(record)
	return db.readRecord(recordPath)
}

// GetAll will get all records
func (db JSON) GetAll() ([]models.Tower, error) {
	recordFilePattern := filepath.Join(db.path, "*.json")
	// Error is only returned for bad patterns which I don't think is very likely here
	// unless some silly person put a bad db.path in there...Users..
	recordFiles, _ := filepath.Glob(recordFilePattern)
	records := make([]models.Tower, 0, len(recordFiles))

	// Short circuit since no records exist
	if len(recordFiles) == 0 {
		return records, nil
	}

	for _, recordFilePath := range recordFiles {
		record, err := db.readRecord(recordFilePath)
		if err != nil {
			// TODO: log a message that a record was unable to be retrieved
		}
		records = append(records, record)
	}
	return records, nil
}
