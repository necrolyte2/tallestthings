package models

import (
	"fmt"
	"strings"
)

// ValidBuildingTypes .
var ValidBuildingTypes = map[string]bool{"TOWER": true, "MAST": true}

// Tower is the main model
type Tower struct {
	Latitude              float64 `json:"latitude"`
	Longitude             float64 `json:"longitude"`
	ArchitecturalHeight   float64 `json:"architecture_height"`
	TipHeight             float64 `json:"tip_height"`
	ObservationHeight     float64 `json:"observation_height"`
	TopFloorHeight        float64 `json:"top_floor_height"`
	FloorCountAboveGround int     `json:"floor_count_above_ground"`
	FloorCountTotal       int     `json:"floor_count_total"`
	BuildingType          string  `json:"building_type"`
	Name                  string  `json:"name"`
}

// Validate is the validation for model
func (t Tower) Validate() error {
	errors := []string{}
	if t.Latitude == 0.0 {
		errors = append(errors, "missing latitude")
	}

	if t.Longitude == 0.0 {
		errors = append(errors, "missing longitude")
	}

	if _, ok := ValidBuildingTypes[t.BuildingType]; !ok {
		errors = append(errors, fmt.Sprintf("%s is not a valid building type", t.BuildingType))
	}

	if t.Name == "" {
		errors = append(errors, "missing name")
	}

	if len(errors) != 0 {
		return fmt.Errorf("invalid tower data: %s", strings.Join(errors, ","))
	}
	return nil
}
